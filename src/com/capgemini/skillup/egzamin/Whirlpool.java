package com.capgemini.skillup.egzamin;

/**
 * Klasa Whirlpool modyfikuje metody klasy Pralka dla marki Whirlpool.
 *
 * @author Rafal Jablonski
 * @version 1.0
 */
public class Whirlpool extends Pralka {
	
	/**
	 * Konstruktor klasy.
	 * Pralka wirpool ma 25 programów prania.
	 */
	public Whirlpool() {
		super();
		super.maxProgram = 25;
		
		// przy nadpisywaniu minimalnego programu trzeba pamiętać o nadpisaniu domyślego programu, jak niżej:
		// super.minProgram = 3;
		// super.setProgram(minProgram);
	}
	
}
