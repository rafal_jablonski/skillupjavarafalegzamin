package com.capgemini.skillup.egzamin;

/**
 * Klasa Amica modyfikuje metody klasy Pralka dla marki Amica.
 *
 * @author Rafal Jablonski
 * @version 1.0
 */
public class Amica extends Pralka {
	
	/**
	 * Konstruktor klasy.
	 */
	public Amica() {
		super();
	}
	
}
