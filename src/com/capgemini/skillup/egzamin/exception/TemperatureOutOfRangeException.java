package com.capgemini.skillup.egzamin.exception;

/**
 * Klasa TemperatureOutOfRangeException.
 * Błąd zwracany w przypadku próby ustawienia temp. poza zakresem.
 *
 * @author Rafal Jablonski
 * @version 1.0
 */
public class TemperatureOutOfRangeException extends RuntimeException {
	
	public TemperatureOutOfRangeException() {
		System.out.println("Temperatura poza zakresem!");
	}
	
	public TemperatureOutOfRangeException(String message) {
		System.out.println(message);
	}
	
}
