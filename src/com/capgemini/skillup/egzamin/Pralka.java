package com.capgemini.skillup.egzamin;

import com.capgemini.skillup.egzamin.exception.TemperatureOutOfRangeException;

/**
 * Klasa Pralka implementuje metody niezbędne do obsługi pralki.
 *
 * @author Rafal Jablonski
 * @version 1.0
 */
public class Pralka {
	
	// numer programu to liczba z zakresu 1-20
	protected int	minProgram	= 1;
	protected int	maxProgram	= 20;
	private int		actualProgram;
	
	// Wartość temperatury to liczbę zmiennoprzecinkową ze skokiem 0,5 stopnia.
	// Temperatura jest z zakresu 0 – 90 stopni.
	protected float	minTemp			= 0.0f;
	protected float	maxTemp			= 90.0f;
	protected float	tempGradation	= 0.5f;
	private float	actualTemp;
	
	// Pralka przechowuje wartość 0 – 1000 predkości wirowania.
	// Skok obrotów o 100.
	protected int	minV		= 0;
	protected int	maxV		= 1000;
	protected int	vGradation	= 100;
	private int		actualV;
	
	/**
	 * Konstruktor klasy.
	 */
	public Pralka() {
		this.actualProgram = minProgram;
		this.actualTemp = minTemp;
		this.actualV = minV;
	}
	
	/**
	 * Ta metoda zaokrągla liczbę o zadany skok.
	 * 
	 * @param number
	 *            liczba do zaokrąglenia
	 * @param gradation
	 *            skok (podziałka)
	 * @return zaokrąglona liczba
	 */
	private float roundByGradation(float number, float gradation) {
		int result = Math.round(number / gradation);
		return result * gradation;
	}
	
	/**
	 * Ta metoda pokazuje komunikat o zmianie temp.
	 */
	private void showTempStatus() {
		System.out.println("Aktualna temperatura: " + this.actualTemp + " \260C");
	}
	
	/**
	 * Ta metoda ustawia nowy program.
	 * 
	 * @param newProgram
	 *            nowy program
	 */
	public void setProgram(int newProgram) {
		if (newProgram >= minProgram && newProgram <= maxProgram) {
			this.actualProgram = newProgram;
		}
	}
	
	/**
	 * Ta metoda zwraca aktualny program.
	 * 
	 * @return aktualny program
	 */
	public int getProgram() {
		return this.actualProgram;
	}
	
	/**
	 * Ta metoda ustawia następny program.
	 * Po przekroczeniu górnej granicy zakresu ustawia wartość min.
	 */
	public void nextProgram() {
		this.actualProgram = (this.actualProgram < maxProgram ? this.actualProgram + 1 : minProgram);
	}
	
	/**
	 * Ta metoda ustawia poprzedni program.
	 * Po przekroczeniu dolnej granicy zakresu ustawia wartość max.
	 */
	public void previousProgram() {
		this.actualProgram = (this.actualProgram > minProgram ? this.actualProgram - 1 : maxProgram);
	}
	
	/**
	 * Ta metoda ustawia nową temp. i wyświetla aktualną temp.
	 * 
	 * @param newTemp
	 *            nowa temp.
	 */
	public void setTemp(float newTemp) {
		if (newTemp >= minTemp && newTemp <= maxTemp) {
			this.actualTemp = roundByGradation(newTemp, tempGradation);
		}
		showTempStatus();
	}
	
	/**
	 * Ta metoda zwraca aktualną temp.
	 * 
	 * @return aktualna temp.
	 */
	public float getTemp() {
		return this.actualTemp;
	}
	
	/**
	 * Ta metoda zwiększa temp. i wyświetla jej aktualną wartość.
	 * 
	 * @throws TemperatureOutOfRangeException
	 *             jeżeli temp. przekracza max. wartość.
	 */
	public void tempUp() throws TemperatureOutOfRangeException {
		if (this.actualTemp < maxTemp) {
			this.actualTemp += tempGradation;
			showTempStatus();
		} else {
			throw new TemperatureOutOfRangeException("Temperatura nie może przekroczyć " + maxTemp + " \260C");
		}
	}
	
	/**
	 * Ta metoda zmniejsza temp. i wyświetla jej aktualną wartość.
	 * 
	 * @throws TemperatureOutOfRangeException
	 *             jeżeli temp. jest poniżej min. wartości.
	 */
	public void tempDown() throws TemperatureOutOfRangeException {
		if (this.actualTemp > minTemp) {
			this.actualTemp -= tempGradation;
			showTempStatus();
		} else {
			throw new TemperatureOutOfRangeException("Temperatura nie może być mniejsza niż " + minTemp + " \260C");
		}
	}
	
	/**
	 * Ta metoda ustawia nową prędkość wirowania.
	 * 
	 * @param newV
	 *            nowa prędkość wirowania.
	 */
	public void setV(int newV) {
		if (newV >= minV && newV <= maxV) {
			this.actualV = (int) roundByGradation(newV, vGradation);
		}
	}
	
	/**
	 * Ta metoda zwraca aktualną prędkość wirowania.
	 * 
	 * @return aktualna prędkość wirowania.
	 */
	public int getV() {
		return this.actualV;
	}
	
	/**
	 * Ta metoda ustawia następną prędkość wirowania.
	 */
	public void upV() {
		this.actualV = (this.actualV < maxV ? this.actualV + vGradation : minV);
	}
	
	/**
	 * Ta metoda ustawia poprzednią prędkość wirowania.
	 */
	public void downV() {
		this.actualV = (this.actualV > minV ? this.actualV - vGradation : maxV);
	}
	
	/**
	 * Ta metoda pokazuje aktualny status pralki.
	 */
	public void showStatus() {
		System.out.println("Numer programu: " + this.actualProgram
				+ ", temp.: " + this.actualTemp
				+ " \260C, prędkość wirowania: "
				+ this.actualV + " obrotów/min.");
	}
}
