package com.capgemini.skillup.egzamin;

/**
 * Klasa Beko modyfikuje metody klasy Pralka dla marki Beko.
 *
 * @author Rafal Jablonski
 * @version 1.0
 */
public class Beko extends Pralka {
	
	/**
	 * Konstruktor klasy.
	 * Pralka beko ma skok temperatury nie o 0,5 stopnia tylko o 1.
	 */
	public Beko() {
		super();
		super.tempGradation = 1.0f;
	}
	
}
