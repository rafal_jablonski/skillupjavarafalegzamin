package com.capgemini.skillup.egzamin;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.skillup.egzamin.exception.TemperatureOutOfRangeException;

/**
 * Klasa BekoTest implementuje testy dla klasy Beko.
 *
 * @author Rafal Jablonski
 * @version 1.0
 */
public class BekoTest {
	
	static Pralka pralka;
	
	@Before
	public void beforeEachTest() {
		pralka = new Beko();
	}
	
	// domyślnie program ustawiony na 1
	@Test
	public void programShouldBeDefaultWhenWasNotChanged() {
		// when program was not changed
		int program = pralka.getProgram();
		// then program should be default: 1
		assertEquals(1, program);
	}
	
	// po ustawieniu nowego programu na 5, powinien zwracać program 5
	@Test
	public void newProgramShouldBeRemembered() {
		// when program was set to 5
		pralka.setProgram(5);
		// then program should be 5
		assertEquals(5, pralka.getProgram());
	}
	
	// próba ustawienia nowego programu powyżej zakresu powinna zostać zignorowana
	@Test
	public void programAboveRangeShouldBeIgnored() {
		// when program was set to 5 and then to 21
		pralka.setProgram(5);
		pralka.setProgram(21);
		// then program should be still 5
		assertEquals(5, pralka.getProgram());
	}
	
	// próba ustawienia nowego programu poniżej zakresu powinna zostać zignorowana
	@Test
	public void programBelowRangeShouldBeIgnored() {
		// when program was set to 5 and then to 0
		pralka.setProgram(5);
		pralka.setProgram(0);
		// then program should be still 5
		assertEquals(5, pralka.getProgram());
	}
	
	// nextProgram() ustawia następny program
	@Test
	public void nextProgramShouldSetNextProgram() {
		// when program was set to 5 and then nextProgram()
		pralka.setProgram(5);
		pralka.nextProgram();
		// then program should be 6
		assertEquals(6, pralka.getProgram());
	}
	
	// jeśli program = 20 to nextProgram() ustawia 1
	@Test
	public void nextProgramShouldSetMinProgramAfterMaxProgram() {
		// when program was set to 20 and then nextProgram()
		pralka.setProgram(20);
		pralka.nextProgram();
		// then program should be 1
		assertEquals(1, pralka.getProgram());
	}
	
	// previousProgram() ustawia poprzedni program
	@Test
	public void previousProgramShouldSetPreviousProgram() {
		// when program was set to 5 and then previousProgram()
		pralka.setProgram(5);
		pralka.previousProgram();
		// then program should be 4
		assertEquals(4, pralka.getProgram());
	}
	
	// jeśli program = 1 to previousProgram() ustawia 20
	@Test
	public void previousProgramShouldSetMaxProgramAfterMinProgram() {
		// when program was set to 1 and then previousProgram()
		pralka.setProgram(1);
		pralka.previousProgram();
		// then program should be 20
		assertEquals(20, pralka.getProgram());
	}
	
	// domyślna temperatura to 0.0
	@Test
	public void tempShouldBeDefaultWhenWasNotChanged() {
		// when temp was not changed
		float temp = pralka.getTemp();
		// then temp should be default: 0.0f
		assertEquals(0.0f, temp, 0.0f);
	}
	
	// po ustawieniu nowej temperatury na 50,0, powinien zwracać temp 50,0
	@Test
	public void newTempShouldBeRemembered() {
		// when temp was set to 50.0
		pralka.setTemp(50.0f);
		// then temp should be 50.0
		assertEquals(50.0f, pralka.getTemp(), 0.0f);
	}
	
	// temperatura powinna być zaokrąglona do 1,0
	@Test
	public void newTempShouldBeRound() {
		// when temp was set to 45.7
		pralka.setTemp(45.7f);
		// then temp should be 46.0
		assertEquals(46.0f, pralka.getTemp(), 0.0f);
		
		// when temp was set to 34.4
		pralka.setTemp(34.4f);
		// then temp should be 34.0
		assertEquals(34.0f, pralka.getTemp(), 0.0f);
	}
	
	// próba ustawienia nowej temp powyżej zakresu powinna zostać zignorowana
	@Test
	public void tempAboveRangeShouldBeIgnored() {
		// when temp was set to 50.0 and then to 90.5
		pralka.setTemp(50.0f);
		pralka.setTemp(90.5f);
		// then temp should be still 50.0
		assertEquals(50.0f, pralka.getTemp(), 0.0f);
	}
	
	// próba ustawienia nowej temp poniżej zakresu powinna zostać zignorowana
	@Test
	public void tempBelowRangeShouldBeIgnored() {
		// when temp was set to 50.0 and then to -0.5
		pralka.setTemp(50.0f);
		pralka.setTemp(-0.5f);
		// then temp should be still 50.0
		assertEquals(50.0f, pralka.getTemp(), 0.0f);
	}
	
	// tempUp() zwiększa temp o 1.0
	@Test
	public void tempUpShouldIncreaseTempOfOneDegree() {
		// when temp was set to 5.0 and then tempUp()
		pralka.setTemp(5.0f);
		pralka.tempUp();
		// then temp should be 6.0
		assertEquals(6.0f, pralka.getTemp(), 0.0f);
	}
	
	// jeśli temp = 90.0 to tempUp() rzuca TemperatureOutOfRangeException
	@Test(expected = TemperatureOutOfRangeException.class)
	public void tempUpShouldThrowExceptionAboveRange() throws TemperatureOutOfRangeException {
		// when temp was set to 90.0 and then tempUp()
		pralka.setTemp(90.0f);
		// then should throw TemperatureOutOfRangeException
		pralka.tempUp();
	}
	
	// tempDown() zmniejsza temp o 0.5
	@Test
	public void tempDownShouldDecreaseTempOfOneDegree() {
		// when temp was set to 5.0 and then tempDown()
		pralka.setTemp(5.0f);
		pralka.tempDown();
		// then temp should be 4.0
		assertEquals(4.0f, pralka.getTemp(), 0.0f);
	}
	
	// jeśli temp = 0.0 to tempDown() rzuca TemperatureOutOfRangeException
	@Test(expected = TemperatureOutOfRangeException.class)
	public void tempDownShouldThrowExceptionBelowRange() throws TemperatureOutOfRangeException {
		// when temp was set to 90.0 and then tempUp()
		pralka.setTemp(0.0f);
		// then should throw TemperatureOutOfRangeException
		pralka.tempDown();
	}
	
	// domyślnie V ustawiony na 0
	@Test
	public void rotationSpeedShouldBeDefaultWhenWasNotChanged() {
		// when rotation speed (V) was not changed
		int v = pralka.getV();
		// then rotation speed (V) should be default: 0
		assertEquals(0, v);
	}
	
	// po ustawieniu nowego V na 500, powinien zwracać V 500
	@Test
	public void newRotationSpeedShouldBeRemembered() {
		// when rotation speed (V) was set to 500
		pralka.setV(500);
		// then rotation speed (V) should be 500
		assertEquals(500, pralka.getV());
	}
	
	// próba ustawienia nowego V powyżej zakresu powinna zostać zignorowana
	@Test
	public void rotationSpeedAboveRangeShouldBeIgnored() {
		// when rotation speed (V) was set to 500 and then to 1001
		pralka.setV(500);
		pralka.setV(1001);
		// then rotation speed (V) should be still 500
		assertEquals(500, pralka.getV());
	}
	
	// próba ustawienia nowego V poniżej zakresu powinna zostać zignorowana
	@Test
	public void rotationSpeedBelowRangeShouldBeIgnored() {
		// when rotation speed (V) was set to 500 and then to -1
		pralka.setV(500);
		pralka.setV(-1);
		// then rotation speed (V) should be still 500
		assertEquals(500, pralka.getV());
	}
	
	// upV() zwiększa V o 100
	@Test
	public void upVShouldIncreaseVByOneHundred() {
		// when rotation speed (V) was set to 500 and then upV()
		pralka.setV(500);
		pralka.upV();
		// then rotation speed (V) should be 600
		assertEquals(600, pralka.getV());
	}
	
	// jeśli V=1000 to upV() ustawia 0
	@Test
	public void upVShouldSetMinVAfterMaxV() {
		// when rotation speed (V) was set to 1000 and then upV()
		pralka.setV(1000);
		pralka.upV();
		// then rotation speed (V) should be 0
		assertEquals(0, pralka.getV());
	}
	
	// downV() zmniejsza V o 100
	@Test
	public void downVShouldDecreaseVByOneHundred() {
		// when rotation speed (V) was set to 500 and then downV()
		pralka.setV(500);
		pralka.downV();
		// then rotation speed (V) should be 400
		assertEquals(400, pralka.getV());
	}
	
	// jeśli V=0 to downV() ustawia 1000
	@Test
	public void downVShouldSetMaxVAfterMinV() {
		// when rotation speed (V) was set to 0 and then downV()
		pralka.setV(0);
		pralka.downV();
		// then rotation speed (V) should be 1000
		assertEquals(1000, pralka.getV());
	}
	
	// V powinno być zaokrąglone do wielokrotności 100
	@Test
	public void newVShouldBeRound() {
		// when rotation speed (V) was set to 250
		pralka.setV(250);
		// then rotation speed (V) should be 300
		assertEquals(300, pralka.getV());
		
		// when rotation speed (V) was set to 249
		pralka.setV(249);
		// then rotation speed (V) should be 200
		assertEquals(200, pralka.getV());
	}
	
}
